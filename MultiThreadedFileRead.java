import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

class MultiThreadedFileRead extends Thread {

	
	String from;	// Path to file	
	FileReader fileReader;
	static Map<String, ArrayList<String>> text = new ConcurrentHashMap<String, ArrayList<String>>();	// Collection for files content
	static Map<String, Map<String, Integer>> text_statistic = new HashMap<String, Map<String, Integer>>();	// Collection for summary statistic with name of the file as key
	static Map<String, Integer> summary_map = new HashMap<String, Integer>();	// Collection for summary statistic 

	MultiThreadedFileRead(String fname) throws Exception {
		fileReader = new FileReader(fname);
		from = fname;
		this.start();
	}

	public static void countWordInCache() {

		// Calculate summary count of words in the files and 
		// count of words in every file separately
		Iterator<String> it = text.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			// Collection for statistic per file
			Map<String, Integer> map = new HashMap<String, Integer>();
			for (String word : text.get(key)) {
				if (!map.containsKey(word)) { 
					map.put(word, 1);
				} else {
					int count = map.get(word);
					map.put(word, count + 1);
				}
				if (!summary_map.containsKey(word)) { 
					summary_map.put(word, 1);
				} else {
					int count = summary_map.get(word);
					summary_map.put(word, count + 1);
				}
			}
			text_statistic.put(key, map);
		}
	}

	public static void printStatistic() {

		List sorted_keys;
		// Get sorted list by count
		sorted_keys = summary_map.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
				.map(Map.Entry::getKey).collect(Collectors.toList());
		// Construct message with summary statistic and statistic by files
		for (int i = 0; i < sorted_keys.size(); i++) {
			String message;
			String word_statistic = " ";

			Iterator<String> it = text_statistic.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				Map<String, Integer> map = new HashMap<String, Integer>();
				int count;
				map = text_statistic.get(key);
				try {
					count = map.get(sorted_keys.get(i));
				} catch (NullPointerException e) {
					count = 0;
				}
				word_statistic = word_statistic + " " + count + " +";
			}
			message = sorted_keys.get(i) + " " + summary_map.get(sorted_keys.get(i)) + " ="
					+ word_statistic.substring(0, word_statistic.length() - 2);
			System.out.println(message);
		}
	}

	public void run() {
		BufferedReader br = null;
		ArrayList<String> words = new ArrayList<String>();
		try {
			br = new BufferedReader(fileReader);
			String buffer = null;
			// Read files line by line
			while ((buffer = br.readLine()) != null) {
				// Remove special characters from the string
				String buffer_clear = buffer.replaceAll("[\\?\\!\\-\\(\\)\\;\\.\\^:,]", "").toLowerCase();
				// Extract every word from the string
				String[] line_of_words = buffer_clear.split("\\s+");
				for (int i = 0; i < line_of_words.length; i++) {
					words.add(line_of_words[i]);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// Associate list of the words with the file
		text.put(from, words);
	}

	// The path to the files is set as args
	public static void main(String args[]) throws Exception {

		// Get the number of the files
		int filesNumber = args.length;
		if (filesNumber < 1) {
			System.out.println("Plz, give me a file for reading");
			System.exit(0);
		}
		MultiThreadedFileRead fr[] = new MultiThreadedFileRead[filesNumber];
		// Start threads for each file
		for (int i = 0; i < filesNumber; i++) {
			fr[i] = new MultiThreadedFileRead(args[i]);
			try {
				fr[i].join();
			} catch (Exception e) {
			}
		}
		countWordInCache();
		printStatistic();
	}
}